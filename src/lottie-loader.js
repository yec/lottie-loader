var loaderUtils = require("loader-utils");

module.exports = function lottieLoader(source) {
  this.cacheable && this.cacheable();
  var value = typeof source === "string" ? JSON.parse(source) : source;
  this.value = [value];
  var output = "var data = " + JSON.stringify(value, undefined, "\t") + ";";

  if (value.assets) {
    value.assets.map(function (asset, i) {
      // require image files
      if (asset.u) {
        var request = loaderUtils.stringifyRequest(this, `./${asset.u + asset.p}`);
        output += `
          data.assets[${i}].file = require(${request});
          data.assets[${i}].split = data.assets[${i}].file.split('/');
          data.assets[${i}].p = data.assets[${i}].split.pop();
          data.assets[${i}].u = data.assets[${i}].split.join('/') + '/';
          delete data.assets[${i}].file;
          delete data.assets[${i}].split;
        `;
      }
      return asset;
    });
  }
  output += "module.exports = data;";
  return output;
}
